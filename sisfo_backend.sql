-- phpMyAdmin SQL Dump
-- version 3.4.3.2
-- http://www.phpmyadmin.net
--
-- Host: 10.126.10.19
-- Generation Time: Apr 09, 2015 at 04:35 PM
-- Server version: 5.0.67
-- PHP Version: 5.2.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `sisfo_backend`
--

-- --------------------------------------------------------

--
-- Table structure for table `mitra_bestari_luar`
--

CREATE TABLE IF NOT EXISTS `mitra_bestari_luar` (
  `nama` varchar(255) NOT NULL,
  `asal_univ` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `spesifikasi_riset` varchar(255) NOT NULL,
  `alamat_email` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `id` int(11) NOT NULL auto_increment,
  `alamat_pengiriman` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `alamat_email` (`alamat_email`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=49 ;

--
-- Dumping data for table `mitra_bestari_luar`
--

INSERT INTO `mitra_bestari_luar` (`nama`, `asal_univ`, `status`, `spesifikasi_riset`, `alamat_email`, `url`, `id`, `alamat_pengiriman`) VALUES
('Gede Rasben Dantes', 'UI', 'OK', 'ERP', 'rasben_dantes@yahoo.com', 'http://http://rasben-dantes.blogspot.com/', 1, NULL),
('Azhari SN, Drs., MT., Dr', 'UGM', 'OK', 'Project Management, Intelligent Agent, Semantic Web, Web Service', 'arisn@ugm.ac.id', 'http://arisn.staff.ugm.ac.id/', 2, NULL),
('Edi Winarko, Drs., M.Sc. Ph.D', 'UGM', 'OK', 'Data Mining, Sistem Informasi', 'ewinarko@ugm.ac.id', 'http://ewinarko.staff.ugm.ac.id', 3, NULL),
('Prof. Dr. Sudradjat, MS.', 'Universitas Padjajaran Bandung', 'OK', 'Optimasi', 'adjat03@yahoo.com', 'http://fmipa.unpad.ac.id/new_fmipa/penelitian-2/', 4, NULL),
('Sri Kusumadewi', 'Universitas Islam Indonesia', 'OK', 'Fuzzy, Fuzzy Neural Network, Artificial Neural Network', 'cicie@fti.uii.ac.id', 'Penulis Buku Fuzzy', 5, NULL),
('Dr. Pri Hermawan', 'Institut Teknologi Bandung', 'OK Responsif', 'Decision Science', 'prihermawan@sbm-itb.ac.id', 'http://www.sbm.itb.ac.id/prihermawan', 6, NULL),
('Achmad Fajar Hendarman, S.T., M.SM', 'Institut Teknologi Bandung', 'OK Responsif', 'Business Process Analysis, Strategic Management, Change Management, Performance & Quality Management, Logistics Systems, Manufacturing Systems, Simulation & Modeling, Business Feasibility Study, People Management, Job Analysis, Work Load Analysis', 'achmad.fajar@sbm-itb.ac.id', 'http://www.sbm.itb.ac.id/achmad-fajar-hendarman-b-eng-msc', 7, NULL),
('Prof. Dr. Ir. Utomo Sarjono Putro, M.Eng', 'Institut Teknologi Bandung', 'OK', 'systems modelling for policy development, agent based modeling, strategic decision making, systems modeling, and decision science', 'utomo@sbm-itb.ac.id', 'http://www.sbm.itb.ac.id/utomo', 8, NULL),
('Dhanan Sarwo Utomo', 'Institut Teknologi Bandung', 'OK Responsif', 'Agent-based Modelling', 'dhanan@sbm-itb.ac.id', 'http://www.researchgate.net/profile/Dhanan_Sarwo_Utomo/', 9, NULL),
('Zaafri Ananto Husodo, Ph.D', 'Universitas Indonesia', 'OK', 'Financial Research, Investment Management', 'z.husodo@gmail.com', 'http://staff.ui.ac.id/z.husodo', 10, NULL),
('Nila Firdausi Nuzula, S.Sos., M.Si., Ph.D', 'Universitas Brawijaya Malang', 'OK', 'Accounting and Financial Management, Corporate Social Responsibility (CSR), Japanese Management', 'nilafirdausi@gmail.com', 'http://lecture.ub.ac.id/anggota/nila/profile/', 11, NULL),
('Dr. Widowati, M.Si', 'Universitas Diponegoro', 'OK', 'Simulasi, Modelling Matematika', 'wiwied_mathundip@yahoo.com', 'http://staff.undip.ac.id/matematika/widowati/biodata/', 12, NULL),
('Drs. Sutimin, M.SI', 'Universitas Diponegoro', 'OK', 'Matematika Terapan', 'su_timin@yahoo.com', 'http://staff.undip.ac.id/matematika/sutimin/biodata/', 13, NULL),
('Aina Musdholifah, S.Kom., M.Kom.', 'Universitas Gadjah Mada Yogyakarta', 'OK Responsif', 'Genetic Algorithm', 'aina_musdholifah@yahoo.com', 'http://cs.ugm.ac.id/computerscience/staff-pengajar/', 14, NULL),
('Prof. Dr. Ing. Ir. Iping Supriana Suwardi DEA', 'Institut Teknologi Bandung', 'OK Responsif', 'Digital', 'iping@informatika.org', 'Pemegang Paten DMR', 15, NULL),
('Dr. Ir. Rinaldi Munir, M.T.', 'Institut Teknologi Bandung', 'OK', 'Cryptography and Security, Image Processing, Numerical Method, and Discrete Mathematics', 'rinaldi-m@stei.itb.ac.id', 'https://www.stei.itb.ac.id/file/stei-script/rinaldi.html', 16, NULL),
('Ir. Wahyu Catur Wibowo, M.Sc., Ph.D.', 'Universitas Indonesia', 'OK', 'Database Management System, Data Warehouse', 'wibowo@cs.ui.ac.id', 'http://staff.ui.ac.id/wibowo', 17, NULL),
('Faizah S.Kom., M.Kom.', 'Universitas Gadjah Mada Yogyakarta', 'OK', 'Sistem Cerdas, Soft computing', 'faizah@ugm.ac.id', 'http://cs.ugm.ac.id/computerscience/staff-pengajar/', 18, NULL),
('Drs. Sri Mulyana, M.Kom.', 'Universitas Gadjah Mada Yogyakarta', 'OK', 'Case-based Reasoning', 'smulyana@ugm.ac.id', 'http://cs.ugm.ac.id/computerscience/2012/05/09/sri-mulyana-m-kom/', 19, NULL),
('Arif Rahman, S.T., M.T.', 'Universitas Brawijaya Malang', 'OK', 'Case-based Reasoning', 'posku@ub.ac.id', 'http://arifindustri.lecture.ub.ac.id/2014/01/sisteminformasi-002/', 20, NULL),
('Leon Andretti Abdillah', 'Universitas Bina Darma', 'OK', 'Information Systems, Information Retriecal, eLearning, Knowledge Management System, Database, DSS, Scientific Publications', 'leon.abdillah@yahoo.com', 'Tidak ada URL', 21, NULL),
('Leo Willyanto Santoso S.Kom., MIT.', 'Universitas Kristen Petra', 'OK Responsif', 'Fuzzy Logic, Algoritma Genetika', 'leow@petra.ac.id', 'Tidak ada URL', 22, NULL),
('Arya Adriansyah', 'Eindhoven University', 'OK', 'Process Mining', 'arya.adriansyah@gmail.com', '-', 23, NULL),
('Zaafri Ananto Husodo, S.E., M.M., Ph.D', 'UI', 'OK', 'Accounting; Financial Management.', 'z.husodo@ui.ac.id; zaafri@cbn.net.id', '-', 24, NULL),
('Nila Firdausi Nuzula, S.Sos., M.Si., Ph.D', 'UB', 'OK', 'Accounting and Financial Management', 'nilafia@ub.ac.id; nilafirdausi@gmail.com', '-', 25, NULL),
('Anifudin Azis, M.Kom.', 'UGM', 'OK', 'Data Mining', 'anifudin@ugm.ac.id', 'http://acadstaff.ugm.ac.id/anifuddin_4', 26, NULL),
('Ari Widyanti, S.T., M.T., Ph.D', 'ITB', 'OK', 'Selection Model, AHP', 'widyanti@mail.ti.itb.ac.id', 'http://www.ti.itb.ac.id/?page_id=427', 27, NULL),
('Ir. Dana Indra Sensuse, MLIS, PhD.', 'UI', 'OK', 'E-Government', 'dana@cs.ui.ac.id', 'http://www.cs.ui.ac.id/id/dana/', 28, NULL),
('Dr. Achmad Nizar Hidayanto, S.Kom., M.Kom', 'UI', 'OK', 'Adopsi Teknologi, E-Government, E-Commerce, Manajemen Pengetahuan, Perilaku Manusia dalam Sistem Informasi, Manajemen Inovasi', 'nizar@cs.ui.ac.id', 'http://www.cs.ui.ac.id/id/nizar/', 29, NULL),
('Satrio Baskoro Yudhoatmojo, S.Kom., M.T.I.', 'UI', 'OK', 'Requirement Engineering', 'satrio.baskoro@cs.ui.ac.id', 'http://www.cs.ui.ac.id/id/satrio-baskoro/', 30, NULL),
('Dinar Mutiara K.N., M.Tech.Com.Info.', 'UNDIP', 'OK', 'Rekayasa Perangkat Lunak', 'dinar.mutiara@gmail.com', 'http://if.undip.ac.id/v1/staff/197601102009122002', 31, NULL),
('Beta Noranita, S.Si., M.Kom.', 'UNDIP', 'OK', 'Sistem Pendukung Keputusan', 'beth2nice@yahoo.com', '-', 32, NULL),
('Puspa Indahati Sandhyaduhita, S.T., M.Sc.', 'UI', 'Ok', 'Business Process Modelling', 'p.indahati@cs.ui.ac.id', 'http://www.cs.ui.ac.id/id/p-indahati/', 33, NULL),
('Putu Wuri Handayani, S.Kom., M.Sc.', 'UI', 'OK', 'Healthcare Information System', 'putu.wuri@cs.ui.ac.id', 'http://www.cs.ui.ac.id/id/putu-wuri/', 34, NULL),
('Sigit Priyanta, S.Si., M.Kom.', 'UGM', 'OK', 'GIS', 'seagatejogja@ugm.ac.id', '-', 35, NULL),
('Diah Priharsari, S.T., M.T.', 'UB', 'OK', 'SCM', 'diah.priharsari@ub.ac.id', 'http://ptiik.ub.ac.id/civitas/detail/130207072055', 36, NULL),
('Gembong Edhi Setyawan, S.T., M.T.', 'UB', 'OK', 'Pemrograman Web', 'gembong@ub.ac.id', 'http://ptiik.ub.ac.id/civitas/detail/201301091124', 37, NULL),
('Wayan Firdaus Mahmudy, S.T., M.T.', 'UB', 'OK', 'Data Mining', 'wayanfm@ub.ac.id', 'http://ptiik.ub.ac.id/civitas/detail/130409072912', 38, NULL),
('Panji Wisnu Wirawan, S.T., M.T.', 'UNDIP', 'OK', 'Rekayasa Perangkat Lunak', 'maspanji@undip.ac.id', 'http://if.undip.ac.id/v1/staff/198104212008121002', 39, NULL),
('Yusi Tyroni Mursityo, S.Kom., M.S.', 'UB', 'OK', 'Rekayasa Perangkat Lunak', 'yusi_tyro@ub.ac.id', 'http://ptiik.ub.ac.id/civitas/detail/201301091150', 40, NULL),
('Fitra Abdurrachman Bachtiar, S.T., M.Eng.', 'UB', 'OK', 'Teknologi dalam Pendidikan', 'fitra.bachtiar@ub.ac.id,', 'http://ptiik.ub.ac.id/civitas/detail/201301091123', 41, NULL),
('Andreas Jodhinata, S.Kom., M.Kom.', 'Universitas Pelita Harapan Surabaya', 'NEW', 'Rekayasa Perangkat Lunak', 'andreas.jodhinata@uphsurabaya.ac.id, andreas.jodhinata@gmail.com', '-', 42, NULL),
('Setiawan Assegaf, Ph.D', 'STIKOM Dinamika Bangsa', 'NEW', 'Knowledge Management System', 'setiawanassegaff@stikom-db.ac.id', '-', 43, NULL),
('Safrian Aswati, S.Kom., M.Kom.', 'STMIK Royal Kisaran', 'NEW', 'unknown', 'alwa_yah@yahoo.com', '-', 44, NULL),
('I Dewa Made Adi Baskara Joni, S.Kom., M.Kom.', 'STMIK STIKOM Indonesia Denpasar (STIKI)', 'NEW', 'Sistem Pendukung Keputusan', 'dewadi.414@gmail.com', '-', 45, NULL),
('Alex Wenda, S.T., M.Eng', 'UIN SUSKA Riau', 'OK', 'Sistem Pakar', 'alexwenda@uin-suska.ac.id', 'http://ee.uin-suska.ac.id/dosen/', 46, NULL),
('Darmawan Baginda Napitupilu', 'LIPI', 'NEW', 'E-Government', 'darwan.na70@gmail.com', '-', 47, NULL),
('Andharini Dwi Cahyani', 'Universitas Trunojoyo', 'NEW', 'DSS, E-Learning, CRM, SCM', 'andharini.dwi.cahyani@gmail.com', '-', 48, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` int(11) NOT NULL,
  PRIMARY KEY  (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `password`, `role`) VALUES
('radityo_pw@is.its.ac.id', '', 0),
('zahra_17@is.its.ac.id', '', 1),
('raras@is.its.ac.id', ' ', 1),
('hanim@is.its.ac.id', '', 1),
('tyas@is.its.ac.id', '', 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
